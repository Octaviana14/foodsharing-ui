import { Injectable } from '@angular/core';
import {User} from '../model/user';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Router} from '@angular/router';

const httpParamsOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private usersUrl: string;

  constructor(private http: HttpClient) { }

  public save(user) {
    const body = JSON.stringify(user);
    console.log(body);
    return this.http.post('/server/api/v1/user/sign-up', body, httpParamsOptions);
  }

  login(user) {
    const body = JSON.stringify(user);
    return this.http.post('/server/api/v1/user/login', body, httpParamsOptions);
  }
}
