import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpParamsOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProducts(id: string) {
    return this.http.get('/server/api/v1/get-products/' + id);
  }

  getOneProduct(id: number) {
    return this.http.get('/server/api/v1/get-single-product/' + id);
  }

  createBikeReg(product) {
    return this.http.post('/server/api/v1/add-product/', product, {observe: 'response'});
  }

  getSearchOptions() {
    return this.http.get('/server/api/v1/get-search-options');
  }
}
