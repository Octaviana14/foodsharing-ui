export class User {
  id: string;
  name: string;
  email: string;
  city: string;
  address: string;
  phone: string;
}
