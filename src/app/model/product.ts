export class Product {
  id: number;
  name: string;
  description: string;
  datePosted: string;
  availableUntil: string;
  userName: string;
  pictureBytes: any;
}
