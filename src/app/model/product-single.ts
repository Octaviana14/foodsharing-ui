import {SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

export class ProductSingle {
  id: number;
  name: string;
  description: string;
  datePosted: string;
  availableUntil: string;
  userName: string;
  category: string;
  pictureBytes: string;
}
