import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({name: 'safeimage'})
export class Safeimage implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  public transform(html) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(html);
  }
}
