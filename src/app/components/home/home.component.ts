import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { ProductService } from '../../services/product.service';
import {Observable} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Product} from '../../model/product';
import {MatSort} from '@angular/material/sort';
import {ActivatedRoute} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  constructor(private productService: ProductService, private route: ActivatedRoute, private sanitizer: DomSanitizer) { }
  public product: any;
  public productReg: any;
  private base64Data: any;
  private retrievedImage: any;
  options: string [];
  filteredOptions: Observable<string[]>;
  public dataSource: any;
  private getOptions;
  displayedColumns: string[] = ['name', 'datePosted', 'userName', 'availableUntil', 'details'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    const routeParams = this.route.snapshot.paramMap;
    const productIdFromRoute = String(routeParams.get('id'));
    this.dataSource = new MatTableDataSource<Product>();
    this.dataSource.sort = this.sort;
    this.getProducts(productIdFromRoute);
  }
  getProducts(id: string) {
    this.productService.getProducts(id).subscribe(
      data => {
        this.dataSource.data = data as Product[];
        console.log(this.dataSource.data);
      },
      err => console.error(err),
      () => console.log('products loaded')
    );
  }
  ngAfterViewInit() {
    const routeParams = this.route.snapshot.paramMap;
    const productIdFromRoute = String(routeParams.get('id'));
    this.getProducts(productIdFromRoute);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getBackground(image): SafeResourceUrl {
    this.retrievedImage = 'data:image/jpeg;base64,' + image;
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.retrievedImage);
  }

  getLinkRef(id: any) {
    location.href = 'http://localhost:4200/view-component/' + id;
  }

  // private _filter(value: string): string [] {
  //   const filterValue = value.toLowerCase();
  //
  //   return this.options.filter(option => option.toLowerCase().includes(filterValue));
  // }
  //
  // getSearchOptions() {
  //   this.productService.getSearchOptions().subscribe(data => {this.getOptions = data; },
  //     err => console.log(err),
  //     () => console.log('search options loaded'));
  //
  // }
}
