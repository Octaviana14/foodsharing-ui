import { Component, OnInit } from '@angular/core';
import {MatFormField} from '@angular/material/form-field';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {Md5} from 'ts-md5';
@Component({
  selector: 'app-signup-after',
  templateUrl: './signup-after.component.html',
  styleUrls: ['./signup-after.component.scss']
})
export class SignupAfterComponent implements OnInit {
  form: FormGroup;
  loginInvalid: boolean;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const passwordmd5 = Md5.hashStr(this.form.value.password).toString();
      this.form.value.password = passwordmd5;
      this.userService.login(this.form.value).subscribe(
        data => {
          this.form.reset();
          return true;
        },
        error => {
        }
      );
    }
  }
}
