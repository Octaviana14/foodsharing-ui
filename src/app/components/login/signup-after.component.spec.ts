import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignupAfterComponent } from './signup-after.component';

describe('SignupAfterComponent', () => {
  let component: SignupAfterComponent;
  let fixture: ComponentFixture<SignupAfterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupAfterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupAfterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
