import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductService} from '../../services/product.service';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Address} from 'ngx-google-places-autocomplete/objects/address';

@Component({
  selector: 'app-add-registration',
  templateUrl: './add-registration.component.html',
  styleUrls: ['./add-registration.component.scss']
})
export class AddRegistrationComponent implements OnInit {

  categories: string[] = [
    'Fructe',
    'Legume',
    'Dulciuri',
    'Lactate, branzeturi si oua',
    'Carne si mezeluri',
    'Mancare gatita',
    'Paste',
    'Altele'
  ];
  cities: string[] = [
    'Iasi',
    'Cluj-Napoca',
    'Bucuresti'
  ];
  bikeform: FormGroup;
  validMessage = '';
  message: any;
  selectedFile: File;
  uploadImageData = new FormData();
  options = {
    componentRestrictions: {
      country: ['RO']
    }
  };
  inputAddress = '';
  constructor(private productService: ProductService, private httpClient: HttpClient) { }

  ngOnInit() {
    this.bikeform = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      availableUntil: new FormControl('', Validators.required),
      category: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      pickupTime: new FormControl(),
    });

  }

  submitRegistration() {
    if (this.bikeform.valid) {
      console.log(this.selectedFile);

      this.uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
      this.uploadImageData.append('formData', JSON.stringify(this.bikeform.value));
      this.validMessage = 'Produsul a fost adaugat. Completati formularul pentru a adauga inca un produs.';
      this.productService.createBikeReg(this.uploadImageData).subscribe(
        data => {
          this.bikeform.reset();
          return true;
        },
        error => {
          return Observable.throw(error);
        }
      );
    } else {
      this.validMessage = 'Please fill out the form';
    }
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onUpload() {
    console.log(this.selectedFile);

    // FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    this.uploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);

    // Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:8080/image/upload', this.uploadImageData, { observe: 'response' })
      .subscribe((response) => {
          if (response.status === 200) {
            this.message = 'Image uploaded successfully';
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
      );

  }


  handleAddressChange(address: any) {
    this.inputAddress = address;
    console.log(address);
  }
}
