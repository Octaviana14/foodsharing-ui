import {Component, OnInit, Pipe} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {ProductSingle} from '../../model/product-single';

@Component({
  selector: 'app-view-registration',
  templateUrl: './view-registration.component.html',
  styleUrls: ['./view-registration.component.scss']
})
export class ViewRegistrationComponent implements OnInit {
  public productReg: any;
  public id: number;
  public base64Data: any;
  public retrievedImage: any;

  constructor(private productService: ProductService, private sanitizer: DomSanitizer, private route: ActivatedRoute) {
  }
  ngOnInit() {
    const routeParams = this.route.snapshot.paramMap;
    this.productReg = new ProductSingle();
    const productIdFromRoute = Number(routeParams.get('id'));
    this.getOneProduct(productIdFromRoute);
  }
  getOneProduct(id: number) {
    this.productService.getOneProduct(id).subscribe(data => {
      this.productReg = data;
      this.base64Data = this.productReg.pictureBytes;
      this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
      this.retrievedImage = this.getBackground(this.retrievedImage);
      console.log(this.retrievedImage);
      console.log(this.productReg);
      },
      err => console.error(err),
      () => console.log('loaded'));
  }

  getBackground(image): SafeResourceUrl {
    return this.sanitizer.bypassSecurityTrustResourceUrl(image);
  }
}
