import { Component, OnInit } from '@angular/core';
import { ProductService} from '../../services/product.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  ngOnInit(): void {
  }
  getLinkRef(id: any) {
    location.href = 'http://localhost:4200/home/' + id;
  }
}
