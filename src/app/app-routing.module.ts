import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './components/admin/admin.component';
import {HomeComponent} from './components/home/home.component';
import {ViewRegistrationComponent} from './components/view-registration/view-registration.component';
import {AddRegistrationComponent} from './components/add-registration/add-registration.component';
import {UserSignupComponent} from './components/user-signup/user-signup.component';
import {SignupAfterComponent} from './components/login/signup-after.component';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent
  },
  {
    path: 'add-registration',
    component: AddRegistrationComponent
  },
  {
    path: 'view-component/:id',
    component: ViewRegistrationComponent
  },
  {
    path: 'home/:id',
    component: HomeComponent
  },
  {
    path: 'sign-up',
    component: UserSignupComponent
  },
  {
    path: 'signup-done',
    component: SignupAfterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
