import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {ProductService} from './services/product.service';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';
import { ViewRegistrationComponent } from './components/view-registration/view-registration.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { AddRegistrationComponent } from './components/add-registration/add-registration.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { UserSignupComponent } from './components/user-signup/user-signup.component';
import { UserListComponent } from './user-list/user-list.component';
import {UserService} from './services/user.service';
import {MatSelectModule} from '@angular/material/select';
import { SignupAfterComponent } from './components/login/signup-after.component';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSliderModule} from '@angular/material/slider';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import {MatButtonModule} from '@angular/material/button';
import {RatingModule} from 'ng-starrating';
import {Safeimage} from './model/safeimage';
import {MatToolbarModule} from '@angular/material/toolbar';
import {ImageViewerModule} from 'ng2-image-viewer';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { AgmCoreModule } from '@agm/core';
import {GoogleMapsModule} from '@angular/google-maps';
import {GooglePlaceModule} from "ngx-google-places-autocomplete";
import {MatSort, MatSortModule} from "@angular/material/sort";

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    HomeComponent,
    ViewRegistrationComponent,
    AddRegistrationComponent,
    UserSignupComponent,
    UserListComponent,
    SignupAfterComponent,
    HeaderComponent,
    FooterComponent,
    Safeimage],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatListModule,
        MatCardModule,
        MatFormFieldModule,
        MatAutocompleteModule,
        FormsModule,
        RatingModule,
        MatSelectModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatSliderModule,
        MatButtonModule,
        MatToolbarModule,
        GoogleMapsModule,
        GooglePlaceModule
    ],
  providers: [ProductService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }

